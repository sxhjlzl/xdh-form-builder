import ElInput from 'element-ui/lib/input'

export default {
  rows: String,
  ...ElInput.props
}
